package main

import (
	_ "embed"
	"github.com/wailsapp/wails"
)

func basic() string {
	return "World!"
}

//go:embed frontend/public/build/bundle.js
var js string

//go:embed frontend/public/build/bundle.css
var css string

func main() {
	timer := new(Timer)
	timer.windowTitle = "Countdown Timer"

	app := wails.CreateApp(&wails.AppConfig{
		Width:  480,
		Height: 240,
		Title:  timer.windowTitle,
		JS:     js,
		CSS:    css,
		Colour: "#131313",
	})

	app.Bind(timer)
	app.Run()
}

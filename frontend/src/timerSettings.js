
export class timerSettings {
    timerValue = ''
    title = ''
    startOnSave = false

    constructor(timerValue, title, startOnSave) {
        this.timerValue = timerValue;
        this.title = title;
        this.startOnSave = startOnSave;
    }
}

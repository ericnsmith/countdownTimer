module countdowntimer

go 1.17

require (
	github.com/gen2brain/beeep v0.0.0-20210529141713-5586760f0cc1
	github.com/wailsapp/wails v1.16.8
)

require (
	github.com/Masterminds/semver v1.4.2 // indirect
	github.com/abadojack/whatlanggo v1.0.1 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/go-playground/colors v1.2.0 // indirect
	github.com/go-toast/toast v0.0.0-20190211030409-01e6764cf0a4 // indirect
	github.com/godbus/dbus/v5 v5.0.3 // indirect
	github.com/gopherjs/gopherjs v0.0.0-20180825215210-0210a2f0f73c // indirect
	github.com/gopherjs/gopherwasm v1.1.0 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/jackmordaunt/icns v1.0.0 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/leaanthony/slicer v1.4.0 // indirect
	github.com/leaanthony/spinner v0.5.3 // indirect
	github.com/leaanthony/synx v0.1.0 // indirect
	github.com/leaanthony/wincursor v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/syossan27/tebata v0.0.0-20180602121909-b283fe4bc5ba // indirect
	github.com/tadvi/systray v0.0.0-20190226123456-11a2b8fa57af // indirect
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8 // indirect
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20190709130402-674ba3eaed22 // indirect
)

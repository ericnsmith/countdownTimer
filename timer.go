package main

import (
	"github.com/gen2brain/beeep"
	"github.com/wailsapp/wails"
	"log"
	"time"
)

type Timer struct {
	wailsRuntime *wails.Runtime
	ticker       *time.Ticker
	windowTitle  string
	timerValue   time.Time
	isRunning    bool
}

const TimeFormat = "15:04:05"

func (t *Timer) WailsInit(runtime *wails.Runtime) error {
	t.wailsRuntime = runtime

	return nil
}

func (t *Timer) IsRunning() bool {
	return t.isRunning
}

func (t *Timer) Start(timeToParse string) {
	var err error

	log.Printf("'Start' called with %s\n", timeToParse)

	if t.isRunning {
		return
	}

	if t.timerValue, err = time.Parse(TimeFormat, timeToParse); err != nil {
		t.emitError("Failed to parse timeToParse argument")
		return
	}

	if err = t.startTicker(); err != nil {
		t.emitError(err.Error())
	}
}

func (t *Timer) Stop() {
	log.Println("'Stop' called")

	if t.isRunning {
		t.isRunning = false
		t.ticker.Stop()
		t.wailsRuntime.Events.Emit("timerStopped", "timer stopped")
	}
}

func (t *Timer) Resume() {
	log.Println("'Resume' called")

	if t.isRunning {
		return
	}

	if err := t.startTicker(); err != nil {
		t.emitError(err.Error())
	}
}

func (t *Timer) SetTitle(newTitle string) {
	log.Println("'SetTitle' called")

	t.windowTitle = newTitle
	t.wailsRuntime.Window.SetTitle(newTitle)
}

func (t *Timer) startTicker() error {
	if t.isDone() {
		t.isRunning = false
		t.emitDone()
		return nil
	}

	t.isRunning = true

	t.ticker = time.NewTicker(1 * time.Second)
	for range t.ticker.C {
		t.timerValue = t.timerValue.Add(-1 * time.Second)
		t.emitTick()

		if t.isDone() {
			t.Stop()
			t.emitDone()

			return beeep.Notify(t.windowTitle, "Countdown timer done", "")
		}
	}

	return nil
}

func (t *Timer) isDone() bool {
	return t.timerValue.Format(TimeFormat) == "00:00:00"
}

func (t *Timer) emitError(msg string) {
	t.wailsRuntime.Events.Emit("error", msg)
}

func (t *Timer) emitDone() {
	t.wailsRuntime.Events.Emit("countDown", "done")
}

func (t *Timer) emitTick() {
	msg := t.timerValue.Format(TimeFormat)
	log.Println("Tick: " + msg)
	t.wailsRuntime.Events.Emit("timerTick", msg)
}
